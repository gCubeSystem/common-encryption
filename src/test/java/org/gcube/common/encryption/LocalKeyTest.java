package org.gcube.common.encryption;

import java.security.InvalidKeyException;

import org.gcube.common.security.providers.SecretManagerProvider;
import org.junit.BeforeClass;
import org.junit.Test;

public class LocalKeyTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		SecretManagerProvider.set(new TestSecret("/gcube/devsec"));
				
	}
	
	@Test
	public void test() throws InvalidKeyException {
		String key=SymmetricKey.getKeyFileName(SecretManagerProvider.get().getContext());
		System.out.println("file key found: "+key);
	}

}
