package org.gcube.common.encryption;

import java.util.Collections;
import java.util.Map;

import org.gcube.common.security.Owner;
import org.gcube.common.security.secrets.Secret;

public class TestSecret extends Secret {

	private String context;
	
	public TestSecret(String context){
		this.context = context;
	}
	
	
	
	@Override
	public Owner getOwner() {
		return null;
	}

	@Override
	public String getContext() {
		return this.context;
	}

	@Override
	public Map<String, String> getHTTPAuthorizationHeaders() {
		return Collections.emptyMap();
	}

	@Override
	public boolean isValid() {
		// TODO Auto-generated method stub
		return false;
	}

}
